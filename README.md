# Lowest Cost Route Searching

## Setup

Install them using the command below:

`pip install -r requirements.txt`

Python 2.7 is recommended and has been tested.

Read [setup.md](./setup.md) for more information on how to effectively manage your git repository and troubleshooting information.

## Overview

Search is an integral part of AI. It helps in problem solving across a wide variety of domains where a solution isn’t immediately clear.  We will implement several graph search algorithms with the goal of solving bi-directional search.


### The Files

While you'll only have to edit and submit **_search_submission.py_**, there are a number of notable files:

1. **_search_submission.py_**: Where We will implement your _PriorityQueue_, _Breadth First Search_, _Uniform Cost Search_, _A* Search_, _Bi-directional Search_
2. **_search_submission_tests.py_**: Sample tests to validate your searches locally.
3. **_romania_graph.pickle_**: Serialized graph files for Romania.
4. **_atlanta_osm.pickle_**: Serialized graph files for Atlanta (optional for robust testing for Race!).
5. **_explorable_graph.py_**: A subclass of `networkx` with explored and neighbor nodes. **FOR DEBUGGING ONLY**
6. **_visualize_graph.py_**: Module to visualize search results.
6. **_osm2networkx.py_**: Module used by visualize graph to read OSM networks.

## The Project

Your task is to implement several informed search algorithms that will calculate a driving route between two points in Romania with a minimal time and space cost.
There is a search_submission_tests file to help We along the way. Your searches should be executed with minimal runtime and memory overhead.

![Atlanta image](atlanta.jpg "Atlanta image")

We will be using an undirected network representing a map of Romania (and an optional Atlanta graph used for the Race!).


### 1: Priority queue


In all searches that involve calculating path cost or heuristic (e.g. uniform-cost), we have to order our search frontier. It turns out the way that we do this can impact our overall search runtime.

To show this, you'll implement a priority queue and demonstrate its performance benefits. For large graphs, sorting all input to a priority queue is impractical. As such, the data structure We implement should have an amortized O(1) insertion and O(lg n) removal time. It should do better than the naive implementation in our tests (InsertionSortQueue), which sorts the entire list after every insertion.


###2: BFS

To get We started with handling graphs, implement and test breadth-first search over the test network.

You'll complete this by writing the "breadth_first_search" method. This returns a path of nodes from a given start node to a given end node, as a list.

For this part, it is optional to use the PriorityQueue as your frontier. We will require it from the next question onwards. We can use it here too if We want to be consistent.

![bfs test routes](bfs_test.png "bfs route test image")


###3: Uniform-cost search


Implement uniform-cost search, using PriorityQueue as your frontier. From now on, PriorityQueue should be your default frontier.

`uniform_cost_search()` should return the same arguments as breadth-first search: the path to the goal node (as a list of nodes).



###4: A* search

Implement A* search using Euclidean distance as your heuristic. You'll need to implement euclidean_dist_heuristic() then pass that function to a_star() as the heuristic parameter. We provide null_heuristic() as a baseline heuristic to test against when calling a_star tests.



###5: Bidirectional uniform-cost search

Implement bidirectional uniform-cost search. Remember that this requires starting your search at both the start and end states.

`bidirectional_ucs()` should return the path from the start node to the goal node (as a list of nodes).


###6: Bidirectional A* search


Implement bidirectional A* search. Remember that We need to calculate a heuristic for both the start-to-goal search and the goal-to-start search.

To test this function, as well as using the provided tests, We can compare the path computed by bidirectional A star to bidirectional ucs search above.
bidirectional_a_star should return the path from the start node to the goal node, as a list of nodes.
