# coding=utf-8
"""
This file is your main submission that will be graded against. Only copy-paste
code on the relevant classes included here. Do not add any classes or functions 
to this file that are not part of the classes that we want.
"""

from __future__ import division

import heapq
import pickle

import os

from copy import deepcopy

class PriorityQueue(object):
    """A queue structure where each element is served in order of priority.

    Elements in the queue are popped based on the priority with higher priority
    elements being served before lower priority elements.  If two elements have
    the same priority, they will be served in the order they were added to the
    queue.

    Traditionally priority queues are implemented with heaps, but there are any
    number of implementation options.

    (Hint: take a look at the module heapq)

    Attributes:
        queue (list): Nodes added to the priority queue.
        current (int): The index of the current node in the queue.
    """

    def __init__(self):
        """Initialize a new Priority Queue.
        """

        self.queue = []

    def pop(self):
        """Pop top priority node from queue.

        Returns:
            The node with the highest priority.
        """

        # TODO: finish this function!
        # raise NotImplementedError

        return heapq.heappop(self.queue)

    def remove(self, node_id):
        """Remove a node from the queue.

        This is a hint, you might require this in ucs,
        however, if you choose not to use it, you are free to
        define your own method and not use it.

        Args:
            node_id (int): Index of node in queue.
        """
        # raise NotImplementedError

        # node = self.queue[node_id][1]
        # self.queue[node_id]= (-9999999,node)
        # heapq.heappop(self.queue)

        self.queue.pop(node_id)
        heapq.heapify(self.queue)

    
    def __iter__(self):
        """Queue iterator.
        """

        return iter(sorted(self.queue))

    def __str__(self):
        """Priority Queuer to string.
        """

        return 'PQ:%s' % self.queue

    def append(self, node):
        """Append a node to the queue.

        Args:
            node: Comparable Object to be added to the priority queue.
        """

        # TODO: finish this function!
        # raise NotImplementedError

        heapq.heappush(self.queue, node)

    def __contains__(self, key):
        """Containment Check operator for 'in'

        Args:
            key: The key to check for in the queue.

        Returns:
            True if key is found in queue, False otherwise.
        """

        return key in [n for _, n in self.queue]

    def __eq__(self, other):
        """Compare this Priority Queue with another Priority Queue.

        Args:
            other (PriorityQueue): Priority Queue to compare against.

        Returns:
            True if the two priority queues are equivalent.
        """

        return self == other

    def size(self):
        """Get the current size of the queue.

        Returns:
            Integer of number of items in queue.
        """

        return len(self.queue)
    
    def clear(self):
        """Reset queue to empty (no nodes).
        """

        self.queue = []
        
    def top(self):
        """Get the top item in the queue.

        Returns:
            The first item stored in teh queue.
        """

        return self.queue[0]


# def breadth_first_search(graph, start, goal):
#     """Warm-up exercise: Implement breadth-first-search.

#     See README.md for exercise description.

#     Args:
#         graph (explorable_graph): Undirected graph to search.
#         start (str): Key for the start node.
#         goal (str): Key for the end node.

#     Returns:
#         The best path as a list from the start and goal nodes (including both).
#     """

#     # TODO: finish this function!
#     # raise NotImplementedError

#     import Queue
#     if start == goal:
#         return []

#     frontier = Queue.Queue()
#     frontier.put([start])
#     explored = []

#     while True:
#         if frontier.empty():
#             return "FAILURE"
#         path = frontier.get()
#         node = path[-1]
#         explored.append(node)

#         for child in graph.neighbors(node):
#             # if (child not in frontier.queue) and (child not in explored):
#             if child not in explored:
#                 curr_path = deepcopy(path)
#                 curr_path.append(child)
#                 if child == goal:
#                     return curr_path
#                 frontier.put(curr_path)



# alternative method using Priority queue
def breadth_first_search(graph, start, goal):
    if start == goal:
        return []

    frontier = PriorityQueue()
    frontier.append((0,start))
    explored = []

    path_dic = {}
    path_dic.update({(0,start):[start]})

    cost = 0

    while True:
        if not frontier:
            return "Failure"
        weight_node = frontier.pop()
        curr_path = path_dic[weight_node] # list
        explored.append(weight_node[1])
        cost += 1
        for child in graph.neighbors(weight_node[1]):
            if (child not in explored) and not (frontier.__contains__(child)):
                path = deepcopy(curr_path)
                path.append(child)
                if child == goal:
                    return path
                frontier.append((cost, child))
                path_dic.update({(cost,child):path})





    # raise NotImplementedError

    # if start == goal:
    #     return []

    # frontier = PriorityQueue()
    # frontier.append((0,{start:[start]}))
    # explored = []

    # while True:
    #     if frontier.empty():
    #         return "FAILURE"
    #     weight_path = frontier.pop()
    #     # node = weight_path[1][-1]
    #     node = weight_path[1].keys()[0]
    #     if node == goal:
    #         return weight_path[1][node]
    #     explored.append(node)

    #     fringe = [] # a list of frontier 
    #     fringe_dic = {}
    #     for f in iter(frontier):
    #         fringe += f[1].keys()
    #         fringe_dic.update(f[1])

    #     for child in graph[node].keys():   
    #         weight = graph[node][child]['weight']
    #         curr_weight_path = deepcopy(weight_path)


    #         if (child not in explored) and (chind not in fringe):
    #             curr_weight_path[0] += weight
    #             curr_weight_path[1][node].append(child)
    #             frontier.append(curr_weight_path)
    #         elif (child in fringe):
    #             existing = 
    #             if (curr_weight_path[0]+weight) < 0
    #                 return 0



def uniform_cost_search(graph, start, goal):
    """Warm-up exercise: Implement uniform_cost_search.

    See README.md for exercise description.

    Args:
        graph (explorable_graph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!

    if start == goal:
        return []

    frontier = PriorityQueue()
    frontier.append((0,start))
    explored = []

    path_dic = {}
    path_dic.update({(0,start):[start]})

    while True:
        if not frontier:
            return "Failure"
        weight_node = frontier.pop()
        curr_path = path_dic[weight_node]

        if weight_node[1] == goal:
            return curr_path

        explored.append(weight_node[1])

        weight = weight_node[0]
        node = weight_node[1]
        for child in graph.neighbors(weight_node[1]):
            weight_child = graph[node][child]['weight']
            path = deepcopy(curr_path)
            path.append(child)
            if (child not in explored) and not (frontier.__contains__(child)):
                frontier.append((weight+weight_child, child))
                path_dic.update({(weight+weight_child, child):path})
            elif frontier.__contains__(child):
                index = 0
                for ext_weight, ext_node in frontier.queue:
                    if ext_node == child and ext_weight > weight+weight_child:
                        frontier.remove(index)
                        frontier.append((weight+weight_child, child))
                        del path_dic[(ext_weight, ext_node)]
                        path_dic.update({(weight+weight_child, child):path})
                    index += 1


def null_heuristic(graph, v, goal ):
    """Null heuristic used as a base line.

    Args:
        graph (explorable_graph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        0
    """

    return 0


def euclidean_dist_heuristic(graph, v, goal):
    """Warm-up exercise: Implement the euclidean distance heuristic.

    See README.md for exercise description.

    Args:
        graph (explorable_graph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        Euclidean distance between `v` node and `goal` node as a list.
    """

    # TODO: finish this function!
    # raise NotImplementedError

    v1 = list(graph.node[v].values()[0])
    v2 = list(graph.node[goal].values()[0])

    distance = (sum([(x-y)**2 for (x, y) in zip(v1,v2)]))**0.5
    return distance



def a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    """ Warm-up exercise: Implement A* algorithm.

    See README.md for exercise description.

    Args:
        graph (explorable_graph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!
    # raise NotImplementedError

    if start == goal:
        return []

    g = 0
    h = heuristic(graph, start, goal)
    f = g+h

    frontier = PriorityQueue()
    frontier.append((f,start))
    explored = []

    path_dic = {}
    path_dic.update({(f,start):[start]})

    while True:
        if not frontier:
            return "Failure"
        weight_node = frontier.pop()
        curr_path = path_dic[weight_node]

        if weight_node[1] == goal:
            return curr_path

        explored.append(weight_node[1])

        weight = weight_node[0] # f value
        node = weight_node[1]
        g_node = weight - heuristic(graph, node, goal)
        for child in graph.neighbors(weight_node[1]):
            weight_child = graph[node][child]['weight']
            path = deepcopy(curr_path)
            path.append(child)
            g = g_node+weight_child
            h = heuristic(graph, child, goal)
            f = g+h

            if (child not in explored) and not (frontier.__contains__(child)):
                frontier.append((f, child))
                path_dic.update({(f, child):path})
            elif frontier.__contains__(child):
                index = 0
                for ext_f, ext_node in frontier.queue:
                    if ext_node == child and ext_f > f:
                        frontier.remove(index)
                        frontier.append((f, child))
                        del path_dic[(ext_f, ext_node)]
                        path_dic.update({(f, child):path})
                    index += 1

def bidirectional_ucs(graph, start, goal, heuristic=euclidean_dist_heuristic):
    """Exercise 1: Bidirectional Search.

    See README.md for exercise description.

    Args:
        graph (explorable_graph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!
    # raise NotImplementedError

    def get_cost(node, path_dic):
        for (c,n) in path_dic.keys():
            if n == node:
                return c

    def get_nodes(frontier):
        nodes = []
        for _, n in frontier.queue:
            nodes.append(n)
        return nodes

    def shortest_path(start, goal, explored, path_dic, explored_R, path_dic_R):
        # frontier_node_set = get_nodes(frontier)
        # frontier_R_node_set = get_nodes(frontier_R)

        # join_set = set(frontier_node_set) & set(frontier_R_node_set)
        join_set = set(explored) & set(explored_R)

        up_bound = float('inf')
        for u in join_set:
            cost = get_cost(u, path_dic)
            cost_R = get_cost(u, path_dic_R)
            if cost+cost_R < up_bound:
                u_optimal = u
                cost_optimal = cost
                cost_R_optimal = cost_R
                up_bound = cost+cost_R
        path_P_optimal = path_dic[(cost_optimal, u_optimal)]
        path_R_optimal = path_dic_R[(cost_R_optimal, u_optimal)]

        path_R_optimal_rev = path_R_optimal[::-1]


        path_optimal = path_P_optimal + path_R_optimal_rev[1:]

        return path_optimal



    if start == goal:
        return []

    frontier = PriorityQueue()
    frontier.append((0, start))
    explored = []
    path_dic = {}
    path_dic.update({(0,start):[start]})

    frontier_R = PriorityQueue()
    frontier_R.append((0,goal))
    explored_R = []
    path_dic_R = {}
    path_dic_R.update({(0,goal):[goal]})

    while True:
        if not frontier and not frontier_R:
            return "Failure"

        if frontier:

            # positive direction
            cost_node = frontier.pop()
            cost = cost_node[0]
            node = cost_node[1]

            curr_path = path_dic[cost_node]

            if cost_node[1] == goal:
                return curr_path

            explored.append(cost_node[1])

            # explore child in positive direction
            for child in graph.neighbors(node):
                weight_child = graph[node][child]['weight']
                path = deepcopy(curr_path)
                path.append(child)
                if (child not in explored) and not (frontier.__contains__(child)):
                    frontier.append((cost+weight_child, child))
                    path_dic.update({(cost+weight_child, child):path})
                elif frontier.__contains__(child):
                    index = 0
                    for ext_cost, ext_node in frontier.queue:
                        if ext_node == child and ext_cost>cost+weight_child:
                            frontier.remove(index)
                            frontier.append((cost+weight_child, child))
                            del path_dic[(ext_cost, ext_node)]
                            path_dic.update({(cost+weight_child, child):path})
                        index += 1
                if child in explored_R: # frontier touches, hence potential optimal
                    explored.append(child)

            if node in explored_R:
                return shortest_path(start, goal, explored, path_dic, explored_R, path_dic_R)

        if frontier_R:

            # reverse direction
            cost_node_R = frontier_R.pop()
            cost_R = cost_node_R[0]
            node_R = cost_node_R[1]

            curr_path_R = path_dic_R[cost_node_R]

            if node_R == start:
                curr_path_R_rev = curr_path_R[::-1]
                return curr_path_R_rev

            explored_R.append(node_R)

            # explore child in reverse direction
            for child_R in graph.neighbors(node_R):
                weight_child_R = graph[node_R][child_R]['weight']
                path_R = deepcopy(curr_path_R)
                path_R.append(child_R)
                if (child_R not in explored_R) and not (frontier_R.__contains__(child_R)):
                    frontier_R.append((cost_R+weight_child_R, child_R))
                    path_dic_R.update({(cost_R+weight_child_R, child_R):path_R})
                elif frontier_R.__contains__(child_R):
                    index_R = 0
                    for ext_cost_R, ext_node_R in frontier_R.queue:
                        if ext_node_R == child_R and ext_cost_R > cost_R + weight_child_R:
                            frontier_R.remove(index_R)
                            frontier_R.append((cost_R+weight_child_R, child_R))
                            del path_dic_R[(ext_cost_R, ext_node_R)]
                            path_dic_R.update({(cost_R+weight_child_R, child_R):path_R})
                        index_R += 1
                if child_R in explored: # frontier touches, hence potential optimal
                    explored_R.append(child_R)

            if node_R in explored:
                return shortest_path(start, goal, explored, path_dic, explored_R, path_dic_R)




def bidirectional_a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    """Exercise 2: Bidirectional A*.

    See README.md for exercise description.

    Args:
        graph (explorable_graph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!
    # raise NotImplementedError

    def get_cost(node, path_dic):
        for (c,n) in path_dic.keys():
            if n == node:
                return c

    def get_nodes(frontier):
        nodes = []
        for _, n in frontier.queue:
            nodes.append(n)
        return nodes

    def shortest_path(graph, start, goal, explored, path_dic, explored_R, path_dic_R):
        # frontier_node_set = get_nodes(frontier)
        # frontier_R_node_set = get_nodes(frontier_R)

        # join_set = set(frontier_node_set) & set(frontier_R_node_set)
        join_set = set(explored) & set(explored_R)

        up_bound = float('inf')
        for u in join_set:
            cost = get_cost(u, path_dic)
            cost_R = get_cost(u, path_dic_R)

            # # according to BiDirectional A Star-slides.pdf in reading resources
            # pi_f = heuristic(graph, u, goal)
            # pi_r = heuristic(graph, u, start)
            # pi_f_full = heuristic(graph, start, goal)

            # p_goal_R = (pi_r - pi_f)/2.0 + pi_f_full/2.0

            # top_f = cost-pi_f
            # top_r = cost_R - pi_r

            g = cost - heuristic(graph, u, goal)
            g_R = cost_R - heuristic(graph, u, start)

            if g+g_R < up_bound:
                u_optimal = u
                cost_optimal = cost
                cost_R_optimal = cost_R
                up_bound = g+g_R
        path_P_optimal = path_dic[(cost_optimal, u_optimal)]
        path_R_optimal = path_dic_R[(cost_R_optimal, u_optimal)]

        path_R_optimal_rev = path_R_optimal[::-1]


        path_optimal = path_P_optimal + path_R_optimal_rev[1:]

        return path_optimal



    if start == goal:
        return []

    g = 0
    h = heuristic(graph, start, goal)
    f = g+h

    frontier = PriorityQueue()
    frontier.append((f, start))
    explored = []
    path_dic = {}
    path_dic.update({(f,start):[start]})

    frontier_R = PriorityQueue()
    frontier_R.append((f,goal))
    explored_R = []
    path_dic_R = {}
    path_dic_R.update({(f,goal):[goal]})

    while True:
        if not frontier and not frontier_R:
            return "Failure"

        if frontier:

            # positive direction
            cost_node = frontier.pop()
            cost = cost_node[0] # f value
            node = cost_node[1]

            g_node = cost - heuristic(graph, node, goal)

            curr_path = path_dic[cost_node]

            if cost_node[1] == goal:
                return curr_path

            explored.append(cost_node[1])

            # explore child in positive direction
            for child in graph.neighbors(node):
                weight_child = graph[node][child]['weight']
                path = deepcopy(curr_path)
                path.append(child)
                g = g_node + weight_child 
                h = heuristic(graph, child, goal)
                f = g+h

                if (child not in explored) and not (frontier.__contains__(child)):
                    frontier.append((f, child))
                    path_dic.update({(f, child):path})
                elif frontier.__contains__(child):
                    index = 0
                    for ext_f, ext_node in frontier.queue:
                        if ext_node == child and ext_f>f:
                            frontier.remove(index)
                            frontier.append((f, child))
                            del path_dic[(ext_f, ext_node)]
                            path_dic.update({(f, child):path})
                        index += 1
                if child in explored_R: # frontier touches, hence potential optimal
                    explored.append(child)

            if node in explored_R:
                return shortest_path(graph, start, goal, explored, path_dic, explored_R, path_dic_R)

        if frontier_R:

            # reverse direction
            cost_node_R = frontier_R.pop()
            cost_R = cost_node_R[0]
            node_R = cost_node_R[1]

            g_node_R = cost_R - heuristic(graph, node_R, start)

            curr_path_R = path_dic_R[cost_node_R]

            if node_R == start:
                curr_path_R_rev = curr_path_R[::-1]
                return curr_path_R_rev

            explored_R.append(node_R)

            # explore child in reverse direction
            for child_R in graph.neighbors(node_R):
                weight_child_R = graph[node_R][child_R]['weight']
                path_R = deepcopy(curr_path_R)
                path_R.append(child_R)

                g_R = g_node_R + weight_child_R
                h_R = heuristic(graph, child_R, start)
                f_R = g_R+h_R

                if (child_R not in explored_R) and not (frontier_R.__contains__(child_R)):
                    frontier_R.append((f_R, child_R))
                    path_dic_R.update({(f_R, child_R):path_R})
                elif frontier_R.__contains__(child_R):
                    index_R = 0
                    for ext_f_R, ext_node_R in frontier_R.queue:
                        if ext_node_R == child_R and ext_f_R > f_R:
                            frontier_R.remove(index_R)
                            frontier_R.append((f_R, child_R))
                            del path_dic_R[(ext_f_R, ext_node_R)]
                            path_dic_R.update({(f_R, child_R):path_R})
                        index_R += 1
                if child_R in explored: # frontier touches, hence potential optimal
                    explored_R.append(child_R)

            if node_R in explored:
                return shortest_path(graph, start, goal, explored, path_dic, explored_R, path_dic_R)



# Extra Credit: Your best search method for the race
#
def load_data():
    """Loads data from data.pickle and return the data object that is passed to
    the custom_search method.

    Will be called only once. Feel free to modify.

    Returns:
         The data loaded from the pickle file.
    """

    pickle_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data.pickle")
    data = pickle.load(open(pickle_file_path, 'rb'))
    return data


def custom_search(graph, start, goal, data=None):
    """Race!: Implement your best search algorithm here to compete against the
    other student agents.

    See README.md for exercise description.

    Args:
        graph (explorable_graph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        data :  Data used in the custom search.
            Default: None.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!
    raise NotImplementedError
